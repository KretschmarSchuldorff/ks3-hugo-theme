# KS3

## Development has ended.

## Overview

A Bootstrap-based responsive theme for [Hugo](https://gohugo.io/), providing a two column view for posts (bloggy entries, that get tagged), and articles (long-form entries, that do not get tagged).

Bloggy posts get the left column, and full entries are presented on the right with a summary.

## Installation

After creating a new site:

```
cd <subdirectory-of-site>
cd themes
git clone https://gitlab.com/KretschmarrSchuldorff/ks3-hugo-theme.git
#Test or use temporarily with:
hugo server -t ks3-hugo-theme
#Use permantently by adding to your config.toml:
theme = "ks3-hugo-theme"
```
## Configuration

See the provided `example-config.toml` on how to configure your site.

## Customizing

The file `static/css/custom.jss` contains all CSS customization used by the theme.

The two `.article-*` classes define the look and feel of the left (meta) column, and the main attraction on the right.

All other classes are overrides of the Bootstrap CSS, with manual precedence by way of `!important` where necessary.

The sole exception to that is the `body` tag styling, which configures the padding for the top `navbar` menu, the `headings-font-family`, but also the background color, which is unchanged by Bootstrap.

## Known Issues

1. The table of contents for articles looks horrible. However, Hugo's templating engine doesn't allow easy customization.

## Todo

- [X] Create a customized color scheme for the `.panel` classes in particular
- [ ] See what we can steal from Material UI for shadows and borders
- [X] Make the views for blog entries and articles better: They are okay right now, but too wide
- [X] Add archetypes
- [X] create an example `example-config.toml`
- [ ] Explore the possibility of using the series taxonomy for sidemenus.
- [ ] Explore the option of adding Bootstrap sidebars to Markdown docs
- [ ] Add social stuff to footers, also copyright and other licenses
