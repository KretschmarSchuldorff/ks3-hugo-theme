1. Read and understand the Code of Conduct.
2. The project is licensed as MIT. That means you give over the copyright to your contributions to the nebulous "project".
3. Take the existing code (both Go's templating code in the {{ mustaches }}, and the more familiar JS and CSS, as found in the templates and use those as your guide to how code should look.
4. If you don't, a reformatting as an extra step is no problem!
5. It's easiest to fork (and, ideally, branch your changes), and submit a pull request than to deal with project membership.